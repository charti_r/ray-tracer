#include <SFML/Graphics.hpp>

int			filter_comics(sf::Image & img)
{
  unsigned short	x, y;
  sf::Vector2u		size(img.getSize());
  sf::Color		pixel;

  x = 0;
  while (x < size.x)
    {
      y = 0;
      while (y < size.y)
	{
	  pixel = img.getPixel(x, y);
	  pixel.r = (short)(pixel.r >> 5) << 5;
	  pixel.g = (short)(pixel.g >> 5) << 5;
	  pixel.b = (short)(pixel.b >> 5) << 5;
	  img.setPixel(x, y, pixel);
	  ++y;
	}
      ++x;
    }
  return (0);
}
