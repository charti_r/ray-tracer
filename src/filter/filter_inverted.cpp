#include <SFML/Graphics.hpp>

int			filter_inverted(sf::Image & img)
{
  unsigned short	x, y;
  sf::Vector2u		size(img.getSize());
  sf::Color		pixel;

  x = 0;
  while (x < size.x)
    {
      y = 0;
      while (y < size.y)
	{
	  pixel = img.getPixel(x, y);
	  pixel.r = 0xFF - pixel.r;
	  pixel.g = 0xFF - pixel.g;
	  pixel.b = 0xFF - pixel.b;
	  img.setPixel(x, y, pixel);
	  ++y;
	}
      ++x;
    }
  return (0);
}
