#include <string>
#include <SFML/Graphics.hpp>

#include "filter.hpp"

int	filter(sf::Image & img, std::string filter)
{
  if (filter.compare(FILTER_NONE) == 0)
    return (0);
  if (filter.compare(FILTER_BLACK) == 0)
    return (filter_black(img));
  if (filter.compare(FILTER_RED) == 0)
    return (filter_red(img));
  if (filter.compare(FILTER_GREEN) == 0)
    return (filter_green(img));
  if (filter.compare(FILTER_BLUE) == 0)
    return (filter_blue(img));
  if (filter.compare(FILTER_INVERTED) == 0)
    return (filter_inverted(img));
  if (filter.compare(FILTER_COMICS) == 0)
    return (filter_comics(img));
  return (-1);
}
