#include <SFML/Graphics.hpp>

int			filter_black(sf::Image & img)
{
  unsigned short	x, y;
  sf::Vector2u		size(img.getSize());
  sf::Color		pixel;
  int			color;

  x = 0;
  while (x < size.x)
    {
      y = 0;
      while (y < size.y)
	{
	  pixel = img.getPixel(x, y);
	  color = (pixel.r + pixel.g + pixel.b) / 3;
	  img.setPixel(x, y, sf::Color(color, color, color));
	  ++y;
	}
      ++x;
    }
  return (0);
}
