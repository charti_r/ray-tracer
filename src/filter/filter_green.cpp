#include <SFML/Graphics.hpp>

int			filter_green(sf::Image & img)
{
  unsigned short	x, y;
  sf::Vector2u		size(img.getSize());
  sf::Color		pixel;

  x = 0;
  while (x < size.x)
    {
      y = 0;
      while (y < size.y)
	{
	  pixel = img.getPixel(x, y);
	  pixel.r = 0x00;
	  pixel.b = 0x00;
	  img.setPixel(x, y, pixel);
	  ++y;
	}
      ++x;
    }
  return (0);
}
