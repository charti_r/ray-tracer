#include <string>
#include <iostream>

#include "err.hpp"

int	interr(const std::string & msg, int value)
{
  std::cerr << msg << std::endl;
  return (value);
}

char	interr(const std::string & msg, char value)
{
  std::cerr << msg << std::endl;
  return (value);
}
