#include <string>
#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "raytracer.hpp"
#include "parsing.hpp"
#include "filter.hpp"
#include "struct.hpp"
#include "const.hpp"
#include "err.hpp"

static int	eventHook(sf::RenderWindow & win, sf::Event & event)
{
  while (win.waitEvent(event))
    {
      if (event.type == sf::Event::Closed)
	return (CLOSE);
      else if (event.type == sf::Event::KeyReleased)
	switch (event.key.code)
	  {
	  case sf::Keyboard::Escape:
	    return (CLOSE);
	  case sf::Keyboard::R:
	    return (REFRESH);
	  case sf::Keyboard::F5:
	    return (REFRESH);
	  case sf::Keyboard::S:
	    return (SAVE);
	  default:
	    return (UNKNOWN);
	  }
    }
  return (UNKNOWN);
}

int	main(int argc, char **argv)
{
  sf::RenderWindow	win(sf::VideoMode(WD, HT), WIN_NAME);
  sf::Sprite		sprite;
  sf::Texture		texture;
  sf::Image		img;
  sf::Event		event;
  t_param		param;
  int			ret = REFRESH;

  if (argc != 2)
    return (interr("raytracer [file]", 1));
  texture.create(WD, HT);
  while (win.isOpen())
    {
      if (ret == REFRESH)
	{
	  win.clear();
	  if (loadFromFile(param, argv[1]) == -1)
	    return (1);
	  displayParam(param);
	  win.setTitle(param.name);
	  img = raytracer(win, texture.copyToImage(), param.scene);
	  filter(img, param.filter);
	  texture.loadFromImage(img);
	  sprite.setTexture(texture);
	  win.draw(sprite);
	  win.display();
	}
      ret = eventHook(win, event);
      switch (ret)
	{
	case CLOSE:
	  win.close();
	  break;
	case SAVE:
	  if (img.saveToFile("image/" + param.name + ".jpg"))
	    std::cout << W_B << (param.name + ".jpg") << W_DF << ": Successfuly saved in " << W_G W_BOLD << "image/" << W_DF << std::endl;
	  break;
	default:
	  break;
	}
    }
  return (0);
}
