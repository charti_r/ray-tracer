#include <SFML/Graphics.hpp>

#include "raytracer.hpp"
#include "struct.hpp"

#include "coord.hpp"
#include "vect.hpp"

Vect		getReflectVect(const Coord & vect,
			       const Coord & pos,
			       const Object *near)
{
  Vect		reflect;
  Coord		normal;
  double	dotProduct;

  reflect.pos = pos;
  normal = near->getNormal(pos);
  dotProduct = -2 * (vect % normal);
  reflect.rot = vect + (dotProduct * normal);
  return (reflect);
}

void		setReflect(sf::Color & color, const t_scene & scene,
			   const Object *near, const Vect & vect,
			   const Coord & pos, int indice)
{
  Vect		reflect;
  Object	*obj;
  sf::Color	objColor;
  Coord		pt;

  reflect = getReflectVect(vect.rot, pos, near);
  if ((obj = getNearObject(scene.objects, reflect, &pt)) != NULL)
    {
      objColor = obj->color;
      setLight(objColor, scene, obj, reflect, pt);
      if (obj->transp > 0.0)
	setTransp(objColor, scene, obj, reflect, pt, scene.itransp - (scene.ireflect - indice));
      if (indice - 1 > 0 && obj->reflect > 0.0)
      	setReflect(objColor, scene, obj, reflect, pt, indice - 1);

      color.r = (color.r * (1 - near->reflect)) + (objColor.r * near->reflect);
      color.g = (color.g * (1 - near->reflect)) + (objColor.g * near->reflect);
      color.b = (color.b * (1 - near->reflect)) + (objColor.b * near->reflect);
    }
  else
    {
      color.r *= (1 - near->reflect);
      color.g *= (1 - near->reflect);
      color.b *= (1 - near->reflect);
    }
}
