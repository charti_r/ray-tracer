#include <string>
#include <vector>
#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "raytracer.hpp"
#include "struct.hpp"
#include "const.hpp"

#include "coord.hpp"
#include "vect.hpp"
#include "object.hpp"

/*
** Return the nearest object from a cam
*/
Object		*getNearObject(const std::vector<Object*> & objects, Vect & vect, Coord *pt)
{
  double	tmp = 0.0, dist = 0.0;
  Object	*near = NULL;
  unsigned int	i;

  i = 0;
  while (i < objects.size())
    {
      tmp = objects[i]->getDistance(vect);
      if (tmp > 0.0 && (near == NULL || tmp < dist))
	{
	  near = objects[i];
	  dist = tmp;
	}
      ++i;
    }
  if (near && pt)
    *pt = vect.pos + (dist * vect.rot);
  return (near);
}

/*
** Return the pixel color to print
*/
static sf::Color	getPixelColor(short x, short y, t_scene & scene)
{
  sf::Color		color;
  Vect			vect;
  Coord			pt;

  vect.pos = scene.cam.pos;
  vect.rot = Coord(x - (WD >> 1), (HT >> 1) - y, DISTANCE);
  vect.rotate(scene.cam.rot);
  if ((scene.near = getNearObject(scene.objects, vect, &pt)) != NULL)
    {
      color = scene.near->color;
      setLight(color, scene, scene.near, vect, pt);
      if (scene.near->transp > 0.0)
      	setTransp(color, scene, scene.near, vect, pt, scene.itransp);
      if (scene.near->reflect > 0.0)
      	setReflect(color, scene, scene.near, vect, pt, scene.ireflect);
    }
  return (color);
}

/*
** Calculate image pixel by pixel
*/
sf::Image		raytracer(sf::RenderWindow & win, sf::Image img, t_scene & scene)
{
  short			x, y;
  short			cent = -1;
  sf::Font		font;
  sf::Text		text;
  sf::FloatRect		textRect;
  sf::RectangleShape	bar;

  font.loadFromFile("resource/font/loading.ttf");
  text.setFont(font);
  text.setCharacterSize(HT >> 2);
  text.setColor(sf::Color::White);
  text.setPosition(WD >> 1, HT >> 1);

  bar.setFillColor(sf::Color::White);
  bar.setPosition(WD >> 3, (HT >> 2) + (HT >> 1));

  x = -1;
  while (++x < WD)
    {
      if ((short)(x * 100.0 / WD) > cent)
	{
	  cent = x * 100.0 / WD;
	  text.setString(convert(cent));
	  textRect = text.getLocalBounds();
	  text.setOrigin(textRect.left + textRect.width / 2, textRect.top + textRect.height / 2);
	  bar.setSize(sf::Vector2f((x * 3) >> 2, 10));
	  win.clear();
	  win.draw(text);
	  win.draw(bar);
	  win.display();
	}
      y = -1;
      while (++y < HT)
	img.setPixel(x, y, getPixelColor(x, y, scene));
    }
  return (img);
}
