#include <cmath>
#include <SFML/Graphics.hpp>

#include "raytracer.hpp"
#include "const.hpp"
#include "struct.hpp"

#include "coord.hpp"
#include "vect.hpp"

void		setLight(sf::Color & color, const t_scene & scene,
			 const Object *near, const Vect & vect,
			 const Coord & pos)
{
  Vect		light;
  Vect		specular;
  Object	*shadow;
  Coord		pt;
  double	cosVect;
  double	tmp[3] = {0};
  double	bright[3] = {0};
  unsigned int	i;

  i = 0;
  while (i < scene.lights.size())
    {
      light = Vect(pos, scene.lights[i]->pos - pos);
      specular = getReflectVect(light.rot, pos, near);
      shadow = getNearObject(scene.objects, light, &pt);
      if (shadow == NULL || (pt - pos).size() > 1)
      	{
	  if ((cosVect = Coord::cosVect(light.rot, near->getNormal(pos))) > 0.0)
	    {
	      tmp[0] += cosVect * scene.lights[i]->color.r * scene.lights[i]->power;
	      tmp[1] += cosVect * scene.lights[i]->color.g * scene.lights[i]->power;
	      tmp[2] += cosVect * scene.lights[i]->color.b * scene.lights[i]->power;
	    }
	  if ((cosVect = Coord::cosVect(vect.rot, specular.rot)) > 0.0)
	    {
	      bright[0] += pow(cosVect, near->spec) * scene.lights[i]->color.r * scene.lights[i]->power * near->bright;
	      bright[1] += pow(cosVect, near->spec) * scene.lights[i]->color.g * scene.lights[i]->power * near->bright;
	      bright[2] += pow(cosVect, near->spec) * scene.lights[i]->color.b * scene.lights[i]->power * near->bright;
	    }
	}
      ++i;
    }
  tmp[0] = color.r * (((tmp[0] + bright[0]) / (double)((i ? i : 1) << 8)) + scene.ambiante);
  color.r = BETWEEN(0x00, tmp[0], 0xFF);
  tmp[1] = color.g * (((tmp[1] + bright[1]) / (double)((i ? i : 1) << 8)) + scene.ambiante);
  color.g = BETWEEN(0x00, tmp[1], 0xFF);
  tmp[2] = color.b * (((tmp[2] + bright[2]) / (double)((i ? i : 1) << 8)) + scene.ambiante);
  color.b = BETWEEN(0x00, tmp[2], 0xFF);
}
