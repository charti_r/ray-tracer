#include <SFML/Graphics.hpp>

#include "raytracer.hpp"
#include "struct.hpp"

#include "coord.hpp"
#include "vect.hpp"

Vect		getTranspVect(const Coord & vect,
			      const Coord & pos,
			      const Object *near)
{
  Vect		transp;

  transp.pos = pos;
  transp.rot = vect;
  return (transp);
}

void		setTransp(sf::Color & color, const t_scene & scene,
			  const Object *near, const Vect & vect,
			  const Coord & pos, int indice)
{
  Vect		transp;
  Object	*obj;
  sf::Color	objColor;
  Coord		pt;

  transp = getTranspVect(vect.rot, pos, near);
  if ((obj = getNearObject(scene.objects, transp, &pt)) != NULL)
    {
      objColor = obj->color;
      setLight(objColor, scene, obj, transp, pt);
      if (obj->reflect > 0.0)
      	setReflect(objColor, scene, obj, transp, pt, scene.ireflect - (scene.itransp - indice));
      if (indice - 1 > 0 && obj->transp > 0.0)
      	setTransp(objColor, scene, obj, transp, pt, indice - 1);

      color.r = (color.r * (1 - near->transp)) + (objColor.r * near->transp);
      color.g = (color.g * (1 - near->transp)) + (objColor.g * near->transp);
      color.b = (color.b * (1 - near->transp)) + (objColor.b * near->transp);
    }
  else
    {
      color.r *= (1 - near->transp);
      color.g *= (1 - near->transp);
      color.b *= (1 - near->transp);
    }
}
