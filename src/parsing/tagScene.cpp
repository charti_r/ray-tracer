#include <string>

#include "parsing.hpp"
#include "struct.hpp"

int	tagScene(t_param & param, t_parse & parse)
{
  int	tagLine = parse.nbLine;
  int	ret = 0;

  while(std::getline(parse.fileStream, parse.read))
    {
      ++parse.nbLine;
      if (getTagName(parse) > 0)
	{
	  if (parse.tag.compare("/" TAG_SCENE) == 0)
	    return (0);
	  else if (parse.tag.compare(PROPERTY_NAME) == 0)
	    param.name = parseString(parse.line);
	  else if (parse.tag.compare(PROPERTY_FILTER) == 0)
	    param.filter = parseString(parse.line);
	  else if (parse.tag.compare(PROPERTY_AMBIANTE) == 0)
	    param.scene.ambiante = BETWEEN(1.0, parseValue(parse.line), 100.0) / 100.0;
	  else if (parse.tag.compare(TAG_CAM) == 0)
	    ret = tagCam(param.scene, parse);
	  else if (parse.tag.compare(TAG_OBJECT) == 0)
	    ret = tagObject(param.scene, parse);
	  else if (parse.tag.compare(TAG_LIGHT) == 0)
	    ret = tagLight(param.scene, parse);
	  else
	    NO_TAG(parse.tag, parse.nbLine, parse.fileName);
	  if (ret == -1) return (-1);
	}
    }
  EOF_TAG(TAG_SCENE, tagLine, parse.fileName);
  return (-1);
}
