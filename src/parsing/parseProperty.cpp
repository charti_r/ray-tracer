#include <string>
#include <SFML/Graphics.hpp>

#include "coord.hpp"

Coord		parseCoord(const std::string & line)
{
  double	x = 0.0, y = 0.0, z = 0.0;
  int		tmp;

  if ((tmp = line.rfind("x=")) != -1)
    x = atof(line.substr(tmp + 2).c_str());
  if ((tmp = line.rfind("y=")) != -1)
    y = atof(line.substr(tmp + 2).c_str());
  if ((tmp = line.rfind("z=")) != -1)
    z = atof(line.substr(tmp + 2).c_str());
  return (Coord(x, y, z));
}

sf::Color	parseColor(const std::string & line)
{
  int		r = 0.0, g = 0.0, b = 0.0;
  int		tmp;

  if ((tmp = line.rfind("r=")) != -1)
    r = strtol(line.substr(tmp + 2).c_str(), NULL, 0);
  else if ((tmp = line.rfind("red=")) != -1)
    r = strtol(line.substr(tmp + 4).c_str(), NULL, 0);
  if ((tmp = line.rfind("g=")) != -1)
    g = strtol(line.substr(tmp + 2).c_str(), NULL, 0);
  else if ((tmp = line.rfind("green=")) != -1)
    g = strtol(line.substr(tmp + 6).c_str(), NULL, 0);
  if ((tmp = line.rfind("b=")) != -1)
    b = strtol(line.substr(tmp + 2).c_str(), NULL, 0);
  else if ((tmp = line.rfind("blue=")) != -1)
    b = strtol(line.substr(tmp + 5).c_str(), NULL, 0);
  return (sf::Color(r, g, b));
}

double		parseValue(const std::string & line)
{
  int		tmp;

  if ((tmp = line.find_first_of("\t ")) != -1)
    if ((tmp = line.find_first_not_of("\t ", tmp)) != -1)
      return (atof(line.substr(tmp).c_str()));
  return (0.0);
}

int		parseIntValue(const std::string & line)
{
  int		tmp;

  if ((tmp = line.find_first_of("\t ")) != -1)
    if ((tmp = line.find_first_not_of("\t ", tmp)) != -1)
      return (atoi(line.substr(tmp).c_str()));
  return (0);
}

std::string	parseString(const std::string & line)
{
  int		pos, len;

  if ((pos = line.find_first_of("\"")) != -1)
    if ((len = line.find_first_of("\"", pos + 1)) != -1)
      return (line.substr(pos + 1, (len - pos) - 1));
  return (std::string());
}
