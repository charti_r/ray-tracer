#include <vector>

#include "object.hpp"
#include "plane.hpp"
#include "parsing.hpp"
#include "struct.hpp"

int	tagPlane(std::vector<Object*> & objects, t_parse & parse)
{
  int	tagLine = parse.nbLine;

  objects.push_back(new Plane());
  while(std::getline(parse.fileStream, parse.read))
    {
      ++parse.nbLine;
      if (getTagName(parse) > 0)
	{
	  if (parse.tag.compare("/" TAG_PLANE) == 0)
	    return (0);
	  else if (parse.tag.compare(PROPERTY_POS) == 0)
	    objects[objects.size() - 1]->pos = parseCoord(parse.line);
	  else if (parse.tag.compare(PROPERTY_ROT) == 0)
	    objects[objects.size() - 1]->rot = parseCoord(parse.line);
	  else if (parse.tag.compare(PROPERTY_COLOR) == 0)
	    objects[objects.size() - 1]->color = parseColor(parse.line);
	  else if (parse.tag.compare(PROPERTY_BRIGHT) == 0)
	    objects[objects.size() - 1]->bright = BETWEEN(0.0, parseValue(parse.line), 100.0) / 100.0;
	  else if (parse.tag.compare(PROPERTY_SPEC) == 0)
	    objects[objects.size() - 1]->spec = BETWEEN(0.0, parseValue(parse.line), 50.0);
	  else if (parse.tag.compare(PROPERTY_TRANSP) == 0)
	    objects[objects.size() - 1]->transp = BETWEEN(0.0, parseValue(parse.line), 100.0) / 100.0;
	  else if (parse.tag.compare(PROPERTY_REFLECT) == 0)
	    objects[objects.size() - 1]->reflect = BETWEEN(0.0, parseValue(parse.line), 100.0) / 100.0;
	  else
	    NO_TAG(parse.tag, parse.nbLine, parse.fileName);
	}
    }
  EOF_TAG(TAG_PLANE, tagLine, parse.fileName);
  return (-1);
}
