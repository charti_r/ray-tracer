#include <string>

#include "light.hpp"
#include "parsing.hpp"
#include "struct.hpp"

int	tagLight(t_scene & scene, t_parse & parse)
{
  int	tagLine = parse.nbLine;

  scene.lights.push_back(new Light());
  while(std::getline(parse.fileStream, parse.read))
    {
      ++parse.nbLine;
      if (getTagName(parse) > 0)
	{
	  if (parse.tag.compare("/" TAG_LIGHT) == 0)
	    return (0);
	  else if (parse.tag.compare(PROPERTY_POS) == 0)
	    scene.lights[scene.lights.size() - 1]->pos = parseCoord(parse.line);
	  else if (parse.tag.compare(PROPERTY_COLOR) == 0)
	    scene.lights[scene.lights.size() - 1]->color = parseColor(parse.line);
	  else if (parse.tag.compare(PROPERTY_POWER) == 0)
	    scene.lights[scene.lights.size() - 1]->power = BETWEEN(0.0, parseValue(parse.line), 100.0) / 100.0;
	  else
	    NO_TAG(parse.tag, parse.nbLine, parse.fileName);
	}
    }
  EOF_TAG(TAG_LIGHT, tagLine, parse.fileName);
  return (-1);
}
