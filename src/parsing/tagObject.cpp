#include <string>

#include "object.hpp"
#include "parsing.hpp"
#include "struct.hpp"

int	tagObject(t_scene & scene, t_parse & parse)
{
  int	tagLine = parse.nbLine;
  int	ret;

  while(std::getline(parse.fileStream, parse.read))
    {
      ++parse.nbLine;
      if (getTagName(parse) > 0)
	{
	  if (parse.tag.compare("/" TAG_OBJECT) == 0)
	    return (0);
	  else if (parse.tag.compare(PROPERTY_ITRANSP) == 0)
	    scene.itransp = parseIntValue(parse.line);
	  else if (parse.tag.compare(PROPERTY_IREFLECT) == 0)
	    scene.ireflect = parseIntValue(parse.line);
	  else if (parse.tag.compare(TAG_PLANE) == 0)
	    ret = tagPlane(scene.objects, parse);
	  else if (parse.tag.compare(TAG_CERCLE) == 0)
	    ret = tagCercle(scene.objects, parse);
	  else if (parse.tag.compare(TAG_SPHERE) == 0)
	    ret = tagSphere(scene.objects, parse);
	  else if (parse.tag.compare(TAG_CYLINDER) == 0)
	    ret = tagCylinder(scene.objects, parse);
	  else if (parse.tag.compare(TAG_CONE) == 0)
	    ret = tagCone(scene.objects, parse);
	  else
	    NO_TAG(parse.tag, parse.nbLine, parse.fileName);
	  if (ret == -1) return (-1);
	}
    }
  EOF_TAG(TAG_OBJECT, tagLine, parse.fileName);
  return (-1);
}
