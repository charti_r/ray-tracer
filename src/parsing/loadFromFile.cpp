#include <cstdlib>
#include <string>

#include "parsing.hpp"
#include "struct.hpp"
#include "err.hpp"

static void	initParam(t_param & param)
{
  param.name = D_NAME;
  param.filter = D_FILTER;
  param.scene.itransp = D_ITRANSP;
  param.scene.ireflect = D_IREFLECT;
  param.scene.ambiante = (D_AMBIANTE / 100.0);
}

static int	parseFile(t_param & param, t_parse & parse)
{
  int		ret = 0;

  param.scene.objects.clear();
  param.scene.lights.clear();
  parse.nbLine = 0;
  while (std::getline(parse.fileStream, parse.read))
    {
      ++parse.nbLine;
      if (getTagName(parse) > 0)
	{
	  if (parse.tag.compare(TAG_SCENE) == 0)
	    ret = tagScene(param, parse);
	  else
	    NO_TAG(parse.tag, parse.nbLine, parse.fileName);
	  if (ret == -1) return (-1);
	}
    }
  return (0);
}

int		loadFromFile(t_param & param, const std::string & fileName)
{
  t_parse	parse;

  parse.fileName = fileName;
  parse.fileStream.open(parse.fileName.c_str());
  initParam(param);
  if (parse.fileStream.is_open() == false)
    return (interr(parse.fileName + ": no such file or directory"));
  system("clear");
  std::cout << W_B << parse.fileName << ": " << W_G W_BOLD << "Loading..." << W_DF << std::endl;
  if (parseFile(param, parse) == -1)
    return (-1);
  parse.fileStream.close();
  std::cout << std::endl;
  return (0);
}

int	getTagName(t_parse & parse)
{
  int	tmp;

  tmp = parse.read.find_first_not_of("\t ");
  parse.line.assign(parse.read, (tmp == -1) ? parse.read.size() : tmp, parse.read.size());
  if (parse.line.size())
    {
      if (parse.line[0] != '<')
	{
	  STX_TAG(parse.line[0], parse.nbLine, parse.fileName);
	  return (-1);
	}
      tmp = parse.line.find_first_not_of("\t <");
      parse.tag.assign(parse.line, tmp, parse.line.find_first_of("\t >", tmp) - tmp);
    }
  return (parse.line.size());
}
