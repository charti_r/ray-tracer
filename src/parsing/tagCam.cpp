#include <string>

#include "vect.hpp"
#include "parsing.hpp"
#include "struct.hpp"

int	tagCam(t_scene & scene, t_parse & parse)
{
  int	tagLine = parse.nbLine;

  while(std::getline(parse.fileStream, parse.read))
    {
      ++parse.nbLine;
      if (getTagName(parse) > 0)
	{
	  if (parse.tag.compare("/" TAG_CAM) == 0)
	    return (0);
	  else if (parse.tag.compare(PROPERTY_POS) == 0)
	    scene.cam.pos = parseCoord(parse.line);
	  else if (parse.tag.compare(PROPERTY_ROT) == 0)
	    scene.cam.rot = parseCoord(parse.line);
	  else
	    NO_TAG(parse.tag, parse.nbLine, parse.fileName);
	}
    }
  EOF_TAG(TAG_CAM, tagLine, parse.fileName);
  return (-1);
}
