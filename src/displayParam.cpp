#include <iostream>

#include "raytracer.hpp"
#include "const.hpp"
#include "vect.hpp"
#include "struct.hpp"

static void	displayCam(Vect & cam)
{
  std::cout << std::endl;
  std::cout << "Camera\n{";
  std::cout << "\n\tPos: " << cam.pos;
  std::cout << "\n\tRot: " << cam.rot;
  std::cout << "\n}" << std::endl;
}

static void	displayObjects(std::vector<Object*> & objects)
{
  unsigned int	i;

  i = -1;
  while (++i < objects.size())
    objects[i]->display();
}

static void	displayLights(std::vector<Light*> & lights)
{
  unsigned int	i;

  i = -1;
  while (++i < lights.size())
    lights[i]->display();
}

void	displayParam(t_param & param)
{
  std::cout << W_G W_BOLD << "Rendering: " << W_DF;
  if (param.name.size())
    std::cout << param.name << std::endl;
  else
    std::cout << WIN_NAME << std::endl;
  std::cout << "Filter: " << param.filter << std::endl;
  std::cout << "Ambiante Light: " << (param.scene.ambiante * 100.0) << std::endl;
  std::cout << "Tranp Indice: " << param.scene.itransp << std::endl;
  std::cout << "Reflect Indice: " << param.scene.ireflect << std::endl;
  displayCam(param.scene.cam);
  displayObjects(param.scene.objects);
  displayLights(param.scene.lights);
}
