#ifndef PLANE_HPP_
# define PLANE_HPP_

# include "object.hpp"
# include "coord.hpp"
# include "vect.hpp"
# include "struct.hpp"

# include <SFML/Graphics.hpp>

class Plane : public Object
{
public:
  Plane(void);

  double	getDistance(Vect vect) const;
  Coord		getNormal(Coord pt) const;

  void		display(void) const;
};

#endif /* !PLANE_HPP_ */
