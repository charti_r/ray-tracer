#include "face.hpp"
#include "const.hpp"

Face::Face(void)
{
  v1 = Vect();
  v2 = Vect();
  v3 = Vect();
  color = sf::Color();
  bright = D_BRIGHT / 100.0;
  spec = D_SPEC;
  transp = D_TRANSP / 100.0;
  reflect = D_REFLECT / 100.0;
}

double		Face::getDistance(Vect vect) const
{
  double	dist = 0.0;

  if (isIntersect(vect) == false)
    return (0.0);
  return (dist);
}

Coord		Face::getNormal(Coord pt) const
{
  return (Coord(0.0, 1.0, 0.0).normalize());
}

void		Face::display(void) const
{
  std::cout << std::endl;
  std::cout << "Face\n{";
  std::cout << "V1: Pos(" << v1.pos << ") Rot(" <<v1.rot << ")";
  std::cout << "V2: Pos(" << v2.pos << ") Rot(" <<v2.rot << ")";
  std::cout << "V3: Pos(" << v3.pos << ") Rot(" <<v3.rot << ")";
  std::cout << "\n\tColor(rgb): " << (int)color.r << " " << (int)color.g << " " << (int)color.b;
  std::cout << "\n\tBright: " << (bright * 100.0);
  std::cout << "\n\tSpec: " << spec;
  std::cout << "\n\tTransp: " << (transp * 100.0);
  std::cout << "\n\tReflect: " << (reflect * 100.0);
  std::cout << "\n}" << std::endl;  
}

bool		Face::isIntersect(const Vect & vect) const
{
  return (true);
}
