#include "vect.hpp"
#include "coord.hpp"

Vect::Vect(const Vect & vect)
{
  pos = vect.pos;
  rot = vect.rot;
}

Vect::Vect(const Coord & setpos, const Coord & setrot)
{
  pos = setpos;
  rot = setrot;
}

void	Vect::simplePos(const Coord & setpos, const Coord & setrot)
{
  pos -= setpos;
  pos.rotate(-setrot);
  rot.rotate(-setrot);
}

void	Vect::rotate(const Coord & setrot)
{
  rot.rotate(setrot);
}
