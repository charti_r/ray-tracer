#ifndef SPHERE_HPP_
# define SPHERE_HPP_

# include "object.hpp"
# include "coord.hpp"
# include "vect.hpp"
# include "struct.hpp"

# include <SFML/Graphics.hpp>

class Sphere : public Object
{
public:
  Sphere(void);

  double	getDistance(Vect vect) const;
  Coord		getNormal(Coord pt) const;
  void		display(void) const;

  double	radius;
};

#endif /* !SPHERE_HPP_ */
