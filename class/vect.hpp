#ifndef VECT_HPP_
# define VECT_HPP_

# include "coord.hpp"

class Vect
{
public:
  Vect(const Coord & pos = Coord(), const Coord & rot = Coord());
  Vect(const Vect & vect);

  void	simplePos(const Coord & pos, const Coord & rot);
  void	rotate(const Coord & rot);

  Coord	pos;
  Coord	rot;
};

#endif /* !VECT_HPP_ */
