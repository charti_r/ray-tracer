#ifndef LIGHT_HPP_
# define LIGHT_HPP_

# include "coord.hpp"

# include <SFML/Graphics.hpp>

class Light
{
public:
  Light(void);

  void		display(void) const;

  Coord		pos;
  sf::Color	color;
  double	power;
};

#endif /* !LIGHT_HPP_ */
