#include "object.hpp"
#include "plane.hpp"
#include "coord.hpp"
#include "vect.hpp"
#include "const.hpp"
#include "struct.hpp"

#include <iostream>
#include <SFML/Graphics.hpp>

Plane::Plane(void):
  Object()
{}

double		Plane::getDistance(Vect vect) const
{
  double	dist = 0.0;

  vect.simplePos(pos, rot);
  if (vect.rot.y)
    dist = -vect.pos.y / vect.rot.y;
  return (dist > DIST_ACC ? dist : 0.0);
}

Coord		Plane::getNormal(Coord pt) const
{
  return (Coord(0.0, 1.0, 0.0).normalize());
}

void		Plane::display(void) const
{
  std::cout << std::endl;
  std::cout << "Plane\n{";
  std::cout << "\n\tPos: " << pos;
  std::cout << "\n\tRot: " << rot;
  std::cout << "\n\tColor(rgb): " << (int)color.r << " " << (int)color.g << " " << (int)color.b;
  std::cout << "\n\tBright: " << (bright * 100.0);
  std::cout << "\n\tSpec: " << spec;
  std::cout << "\n\tTransp: " << (transp * 100.0);
  std::cout << "\n\tReflect: " << (reflect * 100.0);
  std::cout << "\n}" << std::endl;
}
