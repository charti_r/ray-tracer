#include "light.hpp"
#include "coord.hpp"
#include "const.hpp"

#include <iostream>
#include <SFML/Graphics.hpp>

Light::Light()
{
  pos = Coord();
  color = sf::Color();
  power = D_POWER;
}

void		Light::display(void) const
{
  std::cout << std::endl;
  std::cout << "Light\n{";
  std::cout << "\n\tPos: " << pos;
  std::cout << "\n\tColor(rgb): " << (int)color.r << " " << (int)color.g << " " << (int)color.b;
  std::cout << "\n\tPower: " << (power * 100.0);
  std::cout << "\n}" << std::endl;
}
