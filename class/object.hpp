#ifndef OBJECT_HPP_
# define OBJECT_HPP_

# include "coord.hpp"
# include "vect.hpp"

# include <iostream>
# include <SFML/Graphics.hpp>

class Object
{
public:
  Object(void);

  virtual double	getDistance(Vect vect) const = 0;
  virtual Coord		getNormal(Coord pt) const = 0;
  virtual void		display(void) const = 0;

  Coord		pos;
  Coord		rot;
  sf::Color	color;
  double	bright;
  double	spec;
  double	transp;
  double	reflect;
};

#endif /* !OBJECT_HPP_ */
