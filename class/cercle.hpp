#ifndef CERCLE_HPP_
# define CERCLE_HPP_

# include "plane.hpp"
# include "coord.hpp"
# include "vect.hpp"
# include "struct.hpp"

# include <SFML/Graphics.hpp>

class Cercle : public Plane
{
public:
  Cercle(void);

  double	getDistance(Vect vect) const;
  void		display(void) const;

  double	radius;

private:
  double	limit(const Vect & vect, double dist) const;
};

#endif /* !CERCLE_HPP_ */
