#ifndef CYLINDER_HPP_
# define CYLINDER_HPP_

# include "object.hpp"
# include "coord.hpp"
# include "vect.hpp"
# include "struct.hpp"

# include <SFML/Graphics.hpp>

class Cylinder : public Object
{
public:
  Cylinder(void);

  double	getDistance(Vect vect) const;
  Coord		getNormal(Coord pt) const;
  void		display(void) const;

  double	height;
  double	radius;

private:
  double	limit(const Vect & vect, double dist) const;
};

#endif /* !CYLINDER_HPP_ */
