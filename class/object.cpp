#include "object.hpp"
#include "coord.hpp"
#include "vect.hpp"
#include "const.hpp"

#include <SFML/Graphics.hpp>

Object::Object(void)
{
  pos = Coord();
  rot = Coord();
  color = sf::Color();
  bright = D_BRIGHT / 100.0;
  spec = D_SPEC;
  transp = D_TRANSP / 100.0;
  reflect = D_REFLECT / 100.0;
}
