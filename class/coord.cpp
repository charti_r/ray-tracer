#include "coord.hpp"
#include "const.hpp"

#include <iostream>
#include <cmath>

Coord::Coord(void)
{
  x = 0;
  y = 0;
  z = 0;
}

Coord::Coord(const double pos_x, const double pos_y, const double pos_z)
{
  x = pos_x;
  y = pos_y;
  z = pos_z;
}

double	Coord::size(void) const
{
  return (sqrt((x * x) + (y * y) + (z * z)));
}

Coord		Coord::normalize(void)
{
  double	size;

  size = Coord::size();
  x /= size;
  y /= size;
  z /= size;
  return (*this);
}

void	Coord::rotate(const Coord & rot)
{
  double	cosx(cos(RAD(rot.x))), cosy(cos(RAD(rot.y))), cosz(cos(RAD(rot.z)));
  double	sinx(sin(RAD(rot.x))), siny(sin(RAD(rot.y))), sinz(sin(RAD(rot.z)));
  double	tmp;

  // Rot x
  tmp = (y * cosx) - (z * sinx);
  z = (y * sinx) + (z * cosx);
  y = tmp;

  // Rot y
  tmp = (z * cosy) - (x * siny);
  x = (x * cosy) + (z * siny);
  z = tmp;

  // Rot z
  tmp = (x * cosz) - (y * sinz);
  y = (x * sinz) + (y * cosz);
  x = tmp;
}

double	Coord::cosVect(const Coord & v1, const Coord & v2)
{
  return ((v1 % v2) / (v1.size() * v2.size()));
}

/*
** Conditional Operator
*/
bool	Coord::operator==(const Coord & coord) const
{
  return (x == coord.x && y == coord.y && z == coord.z);
}

bool	Coord::operator!=(const Coord & coord) const
{
  return (x != coord.x || y != coord.y || z != coord.z);
}

/*
** Increment Operator
*/
Coord	& Coord::operator++(void)
{
  ++x;
  ++y;
  ++z;
  return (*this);
}

Coord	& Coord::operator--(void)
{
  --x;
  --y;
  --z;
  return (*this);
}

/*
** Shortcut Calcul Operator
*/
Coord	& Coord::operator+=(const Coord & coord)
{
  x += coord.x;
  y += coord.y;
  z += coord.z;
  return (*this);
}

Coord	& Coord::operator+=(const double nb)
{
  x += nb;
  y += nb;
  z += nb;
  return (*this);
}

Coord	& Coord::operator-=(const Coord & coord)
{
  x -= coord.x;
  y -= coord.y;
  z -= coord.z;
  return (*this);
}

Coord	& Coord::operator-=(const double nb)
{
  x -= nb;
  y -= nb;
  z -= nb;
  return (*this);
}

Coord	& Coord::operator*=(const Coord & coord)
{
  x *= coord.x;
  y *= coord.y;
  z *= coord.z;
  return (*this);
}

Coord	& Coord::operator*=(const double nb)
{
  x *= nb;
  y *= nb;
  z *= nb;
  return (*this);
}

Coord	& Coord::operator/=(const Coord & coord)
{
  x /= coord.x;
  y /= coord.y;
  z /= coord.z;
  return (*this);
}

Coord	& Coord::operator/=(const double nb)
{
  x /= nb;
  y /= nb;
  z /= nb;
  return (*this);
}

/*
** Simple Calcul Operator
*/
Coord	operator+(const Coord & a, const Coord & b)
{
  return (Coord(a.x + b.x, a.y + b.y, a.z + b.z));
}

Coord	operator+(const Coord & coord, const double nb)
{
  return (Coord(coord.x + nb, coord.y + nb, coord.z + nb));
}

Coord	operator+(const double nb, const Coord & coord)
{
  return (Coord(nb + coord.x, nb + coord.y, nb + coord.z));
}

Coord	operator-(const Coord & a, const Coord & b)
{
  return (Coord(a.x - b.x, a.y - b.y, a.z - b.z));
}

Coord	operator-(const Coord & coord, const double nb)
{
  return (Coord(coord.x - nb, coord.y - nb, coord.z - nb));
}

Coord	operator-(const double nb, const Coord & coord)
{
  return (Coord(nb - coord.x, nb - coord.y, nb - coord.z));
}

Coord	operator-(const Coord & coord)
{
  return (Coord(-coord.x, -coord.y, -coord.z));
}

Coord	operator*(const Coord & a, const Coord & b)
{
  return (Coord(a.x * b.x, a.y * b.y, a.z * b.z));
}

Coord	operator*(const Coord & coord, const double nb)
{
  return (Coord(coord.x * nb, coord.y * nb, coord.z * nb));
}

Coord	operator*(const double nb, const Coord & coord)
{
  return (Coord(nb * coord.x, nb * coord.y, nb * coord.z));
}

Coord	operator/(const Coord & a, const Coord & b)
{
  return (Coord(a.x / b.x, a.y / b.y, a.z / b.z));
}

Coord	operator/(const Coord & coord, const double nb)
{
  return (Coord(coord.x / nb, coord.y / nb, coord.z / nb));
}

Coord	operator/(const double nb, const Coord & coord)
{
  return (Coord(nb / coord.x, nb / coord.y, nb / coord.z));
}

/*
** DotProduct
*/
double	operator%(const Coord & a, const Coord & b)
{
  return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));
}

/*
** Stream Operator
*/
std::ostream	& operator<<(std::ostream & flux, const Coord & coord)
{
  return (flux << coord.x << "; " << coord.y << "; " << coord.z);
}
