#ifndef FACE_HPP_
# define FACE_HPP_

# include "vect.hpp"

# include <SFML/Graphics.hpp>

class Face
{
public:
  Face(void);

  double	getDistance(Vect vect) const;
  Coord		getNormal(Coord pt) const;
  void		display(void) const;

  Vect		v1;
  Vect		v2;
  Vect		v3;
  sf::Color	color;
  double	bright;
  double	spec;
  double	transp;
  double	reflect;

private:
  // Vect must be normalize
  bool		isIntersect(const Vect & vect) const;
};

#endif /* !FACE_HPP_ */
