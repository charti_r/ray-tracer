#include "object.hpp"
#include "cone.hpp"
#include "coord.hpp"
#include "vect.hpp"
#include "const.hpp"
#include "struct.hpp"

#include <iostream>
#include <cmath>
#include <SFML/Graphics.hpp>

Cone::Cone(void):
  Object()
{
  height = D_HEIGHT;
  slant = RAD(D_SLANT);
  radius = D_RADIUS;
}

double		Cone::getDistance(Vect vect) const
{
  double	a, b, c;
  double	delta;
  double	dist[2];

  vect.simplePos(pos, rot);
  if ((a = 2 * (pow(vect.rot.x, 2) - (pow(vect.rot.y, 2) * slant) + pow(vect.rot.z, 2))) == 0.0)
    return (0.0);
  b = 2 * ((vect.pos.x * vect.rot.x) - (vect.pos.y * vect.rot.y * slant) + (vect.pos.z * vect.rot.z));
  c = pow(vect.pos.x, 2) - (pow(vect.pos.y, 2) * slant) + pow(vect.pos.z, 2);
  c = (radius > 0.0 ? (c - pow(radius, 2)) : (c + pow(radius, 2)));
  if ((delta = pow(b, 2) - (2 * a * c)) < 0.0)
    return (0.0);
  delta = sqrt(delta) / a;
  dist[0] = (-b / a) + delta;
  dist[1] = (-b / a) - delta;
  if (dist[0] > DIST_ACC)
    if (dist[1] > DIST_ACC)
      return (dist[0] > dist[1] ? limit(vect, dist[1]) : limit(vect, dist[0]));
  return (dist[1] > DIST_ACC ? limit(vect, dist[1]) : 0.0);
}

double		Cone::limit(const Vect & vect, double dist) const
{
  Coord		pt;

  pt = vect.pos + (dist * vect.rot);
  return ((IS_BETWEEN(-height, pt.y, 0.0)) ? dist : 0.0);
}

Coord		Cone::getNormal(Coord pt) const
{
  pt -= pos;
  pt.rotate(-rot);
  return (Coord(pt.x, pt.y * -slant, pt.z).normalize());
}

void		Cone::display(void) const
{
  std::cout << std::endl;
  std::cout << "Cone\n{";
  std::cout << "\n\tPos: " << pos;
  std::cout << "\n\tRot: " << rot;
  std::cout << "\n\tColor(rgb): " << (int)color.r << " " << (int)color.g << " " << (int)color.b;
  std::cout << "\n\tBright: " << (bright * 100.0);
  std::cout << "\n\tSpec: " << spec;
  std::cout << "\n\tTransp: " << (transp * 100.0);
  std::cout << "\n\tReflect: " << (reflect * 100.0);
  std::cout << "\n\tHeight: " << height;
  std::cout << "\n\tSlant: " << DEG(slant);
  std::cout << "\n\tRadius: " << radius;
  std::cout << "\n}" << std::endl;
}
