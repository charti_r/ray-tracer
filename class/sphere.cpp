#include "object.hpp"
#include "sphere.hpp"
#include "coord.hpp"
#include "vect.hpp"
#include "const.hpp"
#include "struct.hpp"

#include <iostream>
#include <cmath>
#include <SFML/Graphics.hpp>

Sphere::Sphere(void):
  Object()
{
  radius = D_RADIUS;
}

double		Sphere::getDistance(Vect vect) const
{
  double	a, b, c;
  double	delta;
  double	dist[2];

  vect.simplePos(pos, rot);
  if ((a = 2 * (pow(vect.rot.x, 2) + pow(vect.rot.y, 2) + pow(vect.rot.z, 2))) == 0.0)
    return (0.0);
  b = 2 * ((vect.pos.x * vect.rot.x) + (vect.pos.y * vect.rot.y) + (vect.pos.z * vect.rot.z));
  c = pow(vect.pos.x, 2) + pow(vect.pos.y, 2) + pow(vect.pos.z, 2) - pow(radius, 2);
  if ((delta = pow(b, 2) - (2 * a * c)) < 0.0)
    return (0.0);
  delta = sqrt(delta) / a;
  dist[0] = (-b / a) + delta;
  dist[1] = (-b / a) - delta;
  if (dist[0] > DIST_ACC)
    if (dist[1] > DIST_ACC)
      return (dist[0] > dist[1] ? dist[1] : dist[0]);
  return (dist[1] > DIST_ACC ? dist[1] : 0.0);
}

Coord		Sphere::getNormal(Coord pt) const
{
  pt -= pos;
  pt.rotate(-rot);
  return (pt.normalize());
}

void		Sphere::display(void) const
{
  std::cout << std::endl;
  std::cout << "Sphere\n{";
  std::cout << "\n\tPos: " << pos;
  std::cout << "\n\tRot: " << rot;
  std::cout << "\n\tColor(rgb): " << (int)color.r << " " << (int)color.g << " " << (int)color.b;
  std::cout << "\n\tBright: " << (bright * 100.0);
  std::cout << "\n\tSpec: " << spec;
  std::cout << "\n\tTransp: " << (transp * 100.0);
  std::cout << "\n\tReflect: " << (reflect * 100.0);
  std::cout << "\n\tRadius: " << radius;
  std::cout << "\n}" << std::endl;
}
