#ifndef COORD_HPP_
# define COORD_HPP_

# include <iostream>

class Coord
{
public:
  Coord(void);
  Coord(const double pos_x, const double pos_y, const double pos_z);

  double	size(void) const;
  Coord		normalize(void);
  void		rotate(const Coord & rot);

  static double	cosVect(const Coord & v1, const Coord & v2);

  bool		operator==(const Coord & coord) const;
  bool		operator!=(const Coord & coord) const;

  Coord		& operator++(void);
  Coord		& operator--(void);

  Coord		& operator+=(const Coord & coord);
  Coord		& operator+=(const double nb);
  Coord		& operator-=(const Coord & coord);
  Coord		& operator-=(const double nb);
  Coord		& operator*=(const Coord & coord);
  Coord		& operator*=(const double nb);
  Coord		& operator/=(const Coord & coord);
  Coord		& operator/=(const double nb);

  friend Coord	operator+(const Coord & a, const Coord & b);
  friend Coord	operator+(const Coord & coord, const double nb);
  friend Coord	operator+(const double nb, const Coord & coord);
  friend Coord	operator-(const Coord & a, const Coord & b);
  friend Coord	operator-(const Coord & coord, const double nb);
  friend Coord	operator-(const double nb, const Coord & coord);
  friend Coord	operator-(const Coord & coord);
  friend Coord	operator*(const Coord & a, const Coord & b);
  friend Coord	operator*(const Coord & coord, const double nb);
  friend Coord	operator*(const double nb, const Coord & coord);
  friend Coord	operator/(const Coord & a, const Coord & b);
  friend Coord	operator/(const Coord & coord, const double nb);
  friend Coord	operator/(const double nb, const Coord & coord);
  friend double	operator%(const Coord & a, const Coord & b);

  friend std::ostream	& operator<<(std::ostream & flux, const Coord & coord);

  double	x;
  double	y;
  double	z;
};

#endif /* !COORD_HPP_ */
