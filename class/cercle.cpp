#include "plane.hpp"
#include "cercle.hpp"
#include "coord.hpp"
#include "vect.hpp"
#include "const.hpp"
#include "struct.hpp"

#include <cmath>
#include <iostream>
#include <SFML/Graphics.hpp>

Cercle::Cercle(void):
  Plane()
{
  radius = D_RADIUS;
}

double		Cercle::getDistance(Vect vect) const
{
  double	dist = 0.0;

  vect.simplePos(pos, rot);
  if (vect.rot.y)
    dist = -(vect.pos.y / vect.rot.y);
  return (limit(vect, dist) > DIST_ACC ? limit(vect, dist) : 0.0);
}

double		Cercle::limit(const Vect & vect, double dist) const
{
  Coord		pt;

  pt = vect.pos + (dist * vect.rot);
  if (sqrt(pow(pt.x, 2) + pow(pt.z, 2)) < radius)
    return (dist);
  return (0.0);
}

void		Cercle::display(void) const
{
  std::cout << std::endl;
  std::cout << "Cercle\n{";
  std::cout << "\n\tPos: " << pos;
  std::cout << "\n\tRot: " << rot;
  std::cout << "\n\tColor(rgb): " << (int)color.r << " " << (int)color.g << " " << (int)color.b;
  std::cout << "\n\tBright: " << (bright * 100.0);
  std::cout << "\n\tSpec: " << spec;
  std::cout << "\n\tTransp: " << (transp * 100.0);
  std::cout << "\n\tReflect: " << (reflect * 100.0);
  std::cout << "\n\tRadius: " << radius;
  std::cout << "\n}" << std::endl;
}
