#ifndef CONE_HPP_
# define CONE_HPP_

# include "object.hpp"
# include "coord.hpp"
# include "vect.hpp"
# include "struct.hpp"

# include <SFML/Graphics.hpp>

class Cone : public Object
{
public:
  Cone(void);

  double	getDistance(Vect vect) const;
  Coord		getNormal(Coord pt) const;
  void		display(void) const;

  double	height;
  double	slant;
  double	radius;

private:
  double	limit(const Vect & vect, double dist) const;
};

#endif /* !CONE_HPP_ */
