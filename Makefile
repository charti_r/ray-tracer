CPPFLAGS	+= -W -Wall -pedantic -o3
CPPFLAGS	+= -I./header/ -I./class/

CC		= g++

RM		= rm -f

NAME		= raytracer

RELEASE		= Ray\ Tracer
RESRC		= resource

LIB		+= -lm
LIB		+= -lsfml-graphics -lsfml-window -lsfml-system

# Source
SRC		+= src/main.cpp \
		   src/convert.cpp \
		   src/displayParam.cpp \
		   src/err.cpp \
		   src/rt/raytracer.cpp \
		   src/rt/light.cpp \
		   src/rt/transp.cpp \
		   src/rt/reflect.cpp

# Class
SRC		+= class/coord.cpp \
		   class/vect.cpp \
		   class/face.cpp \
		   class/object.cpp \
		   class/light.cpp \
		   class/plane.cpp \
		   class/cercle.cpp \
		   class/sphere.cpp \
		   class/cylinder.cpp \
		   class/cone.cpp

# Filter
SRC		+= src/filter/filter.cpp \
		   src/filter/filter_black.cpp \
		   src/filter/filter_red.cpp \
		   src/filter/filter_green.cpp \
		   src/filter/filter_blue.cpp \
		   src/filter/filter_inverted.cpp \
		   src/filter/filter_comics.cpp

# Parsing
SRC		+= src/parsing/loadFromFile.cpp \
		   src/parsing/parseProperty.cpp \
		   src/parsing/tagScene.cpp \
		   src/parsing/tagCam.cpp \
		   src/parsing/tagObject.cpp \
		   src/parsing/tagPlane.cpp \
		   src/parsing/tagCercle.cpp \
		   src/parsing/tagSphere.cpp \
		   src/parsing/tagCylinder.cpp \
		   src/parsing/tagCone.cpp \
		   src/parsing/tagLight.cpp

OBJ		= $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(CPPFLAGS) $(LIB) -o $(NAME)

release: $(OBJ)
	$(CC) $(OBJ) $(CPPFLAGS) $(LIB) -o $(RELEASE)/$(NAME)
	mkdir $(RELEASE)/$(RESRC)
	cp -r $(RESRC)/lib/ $(RELEASE)/$(RESRC)
	cp -r $(RESRC)/font/ $(RELEASE)/$(RESRC)
	cp -r $(RESRC)/scene/ $(RELEASE)
	cp $(RESRC)/doc $(RELEASE)

unrelease:
	$(RM) $(RELEASE)/$(NAME)
	$(RM) -r $(RELEASE)/$(RESRC)
	$(RM) -r $(RELEASE)/scene/
	$(RM) $(RELEASE)/doc

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(RELEASE)/$(NAME)
	$(RM) -r $(RELEASE)/$(RESRC)
	$(RM) -r $(RELEASE)/scene/
	$(RM) $(RELEASE)/doc

re: fclean all

.PHONY: all release unrelease clean fclean re
