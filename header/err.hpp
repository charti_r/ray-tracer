#ifndef ERR_HPP_
# define ERR_HPP_

# include <string>

int	interr(const std::string & msg, int value = -1);
char	charerr(const std::string & msg, char value = -1);

#endif /* !ERR_HPP_ */
