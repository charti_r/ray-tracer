#ifndef FILTER_HPP_
# define FILTER_HPP_

# define FILTER_NONE		"none"
# define FILTER_BLACK		"black"
# define FILTER_RED		"red"
# define FILTER_GREEN		"green"
# define FILTER_BLUE		"blue"
# define FILTER_INVERTED	"inverted"
# define FILTER_COMICS		"comics"

int	filter(sf::Image & img, std::string filter);
int	filter_black(sf::Image & img);
int	filter_red(sf::Image & img);
int	filter_green(sf::Image & img);
int	filter_blue(sf::Image & img);
int	filter_inverted(sf::Image & img);
int	filter_comics(sf::Image & img);

#endif /* !FILTER_HPP_ */
