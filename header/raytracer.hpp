#ifndef RAYTRACER_HPP_
# define RAYTRACER_HPP_

# include <string>

# include <SFML/Window.hpp>
# include <SFML/Graphics.hpp>

# include "object.hpp"
# include "vect.hpp"
# include "struct.hpp"

std::string	convert(const int nb);
void		displayParam(t_param & param);

sf::Image	raytracer(sf::RenderWindow & win, sf::Image img, t_scene & scene);
Object		*getNearObject(const std::vector<Object*> & objects, Vect & vect, Coord *pt = NULL);

/*
** Light
*/
void		setLight(sf::Color & color, const t_scene & scene,
			 const Object *near, const Vect & vect,
			 const Coord & pos);

/*
** Transp
*/
Vect		getTranspVect(const Coord & vect,
			      const Coord & pos,
			      const Object *near);
void		setTransp(sf::Color & color, const t_scene & scene,
			  const Object *near, const Vect & vect,
			  const Coord & pos, int indice);

/*
** Reflect
*/
Vect		getReflectVect(const Coord & vect,
			       const Coord & pos,
			       const Object *near);
void		setReflect(sf::Color & color, const t_scene & scene,
			   const Object *near, const Vect & vect,
			   const Coord & pos, int indice);

#endif /* !RAYTRACER_HPP_ */
