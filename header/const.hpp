#ifndef CONST_HPP_
# define CONST_HPP_

# include <cmath>

/*
** Key Hook Return
*/
# define UNKNOWN	0
# define CLOSE		(1 << 0)
# define REFRESH	(1 << 1)
# define SAVE		(1 << 2)

/*
** Style Writting
*/
# define W_DF		"\033[0;39m"
# define W_BOLD		"\033[1m"
# define W_UNDER	"\033[4m"
# define W_R		"\033[31m"
# define W_G		"\033[32m"
# define W_B		"\033[34m"

# define WIN_NAME	"Ray Tracer"
# define WD		1240
# define HT		720
# define DISTANCE	1024
# define DIST_ACC	0.1

/*
** Default Values
*/
# define D_NAME		"Ray Tracer -- CalvinDead"
# define D_FILTER	"none"
# define D_ITRANSP	4
# define D_IREFLECT	3
# define D_BRIGHT	20
# define D_SPEC		20
# define D_TRANSP	0
# define D_REFLECT	0
# define D_AMBIANTE	20
# define D_POWER	100
# define D_HEIGHT	200
# define D_SLANT	45
# define D_RADIUS	200

# define VISION_VECT(x, y)	(Coord(x - (WD >> 1), (HT >> 1) - y, DISTANCE))

/*
** Angle Convertion
*/
# define RAD(x)		(M_PI * (x) / 180)
# define DEG(x)		((x) * 180 / M_PI)

/*
** Result is the var between min and max
*/
# define IS_BETWEEN(min, var, max)	(((var) < (min)) ? (false) : ((var) < (max)))
# define BETWEEN(min, var, max)		(((var) < (min)) ? (min) : (((var) > (max)) ? (max) : (var)))

#endif /* !CONST_HPP_ */
