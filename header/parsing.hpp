#ifndef PARSING_HPP_
# define PARSING_HPP_

# include <iostream>
# include <string>
# include <fstream>

# include "const.hpp"
# include "struct.hpp"
# include "coord.hpp"

/*
** XML Parsing
*/
# define TAG_SCENE		"scene"
# define TAG_CAM		"cam"
# define TAG_OBJECT		"object"
# define TAG_PLANE		"plane"
# define TAG_CERCLE		"cercle"
# define TAG_SPHERE		"sphere"
# define TAG_CYLINDER		"cylinder"
# define TAG_CONE		"cone"
# define TAG_LIGHT		"light"

# define PROPERTY_NAME		"name"
# define PROPERTY_FILTER	"filter"
# define PROPERTY_ITRANSP	"itransp"
# define PROPERTY_IREFLECT	"ireflect"
# define PROPERTY_AMBIANTE	"ambiante"
# define PROPERTY_POS		"pos"
# define PROPERTY_ROT		"rot"
# define PROPERTY_COLOR		"color"
# define PROPERTY_BRIGHT	"bright"
# define PROPERTY_SPEC		"spec"
# define PROPERTY_TRANSP	"transp"
# define PROPERTY_REFLECT	"reflect"
# define PROPERTY_POWER		"power"
# define PROPERTY_HEIGHT	"height"
# define PROPERTY_SLANT		"slant"
# define PROPERTY_RADIUS	"radius"

# define BEG_COMMENT		"<!--"
# define END_COMMENT		"-->"

# define SET_TAG(tag)		(std::cout << W_BOLD W_UNDER W_G << (tag) << W_DF << ": has been set" << std::endl)
# define STX_TAG(c, l, fname)	(std::cerr << W_B << (fname) << W_DF << ": syntax error (l." << (l) << "): expected '<' instead '" << W_BOLD W_R << (c) << W_DF << "'" << std::endl)
# define NO_TAG(tag, l, fname)	(std::cerr << W_B << (fname) << W_DF << ": tag error (l." << (l) << "): tag <" << W_BOLD W_R << (tag) << W_DF << "> ignored" << std::endl)
# define EOF_TAG(tag, l, fname)	(std::cerr << W_B << (fname) << W_DF << ": unexpected End Of File (l." << (l) << "): tag <" << W_BOLD W_R << (tag) << W_DF << "> must be closed by </" << (tag) << ">" << std::endl)

typedef struct	s_parse
{
  std::string	fileName;
  std::ifstream	fileStream;
  std::string	read;
  std::string	line;
  std::string	tag;
  int		nbLine;
}		t_parse;

int	loadFromFile(t_param & param, const std::string & fileName);
int	getTagName(t_parse & parse);
int	tagScene(t_param & param, t_parse & parse);

int	tagCam(t_scene & scene, t_parse & parse);
int	tagObject(t_scene & scene, t_parse & parse);
int	tagLight(t_scene & scene, t_parse & parse);

int	tagPlane(std::vector<Object*> & objects, t_parse & parse);
int	tagCercle(std::vector<Object*> & objects, t_parse & parse);
int	tagSphere(std::vector<Object*> & objects, t_parse & parse);
int	tagCylinder(std::vector<Object*> & objects, t_parse & parse);
int	tagCone(std::vector<Object*> & objects, t_parse & parse);

Coord		parseCoord(const std::string & line);
sf::Color	parseColor(const std::string & line);
double		parseValue(const std::string & line);
int		parseIntValue(const std::string & line);
std::string	parseString(const std::string & line);

#endif /* !PARSING_HPP_ */
