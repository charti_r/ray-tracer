#ifndef STRUCT_HPP_
# define STRUCT_HPP_

# include <string>
# include <vector>

# include "coord.hpp"
# include "vect.hpp"
# include "object.hpp"
# include "light.hpp"

typedef struct		s_scene
{
  Vect			cam;
  std::vector<Object*>	objects;
  std::vector<Light*>	lights;
  int			itransp;
  int			ireflect;
  double		ambiante;
  Object		*near;
}			t_scene;

typedef struct		s_param
{
  t_scene		scene;
  std::string		name;
  std::string		filter;
}			t_param;

#endif /* !STRUCT_HPP_ */
