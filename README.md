# To do list #

### Parsing ###
* UpDate XML to be able to write properties in parent tag
* Parse OBJ format

### Application ###
* Modify light class
* Add light roof and light spot
* Add direct light
* Add tore and void cube
* Add XYZ limitations
* Set a face from points (OBJ format compatibility)
* Add optical indice to modify transp and reflect vectors
* Add time and movement property